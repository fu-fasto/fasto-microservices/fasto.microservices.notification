﻿using FastO.Microservices.Notification.Services;
using Microsoft.AspNetCore.Mvc;

namespace FastO.Microservices.Notification.Controllers
{
    [ApiController]
    [Route("health")]
    public class HealthController : ControllerBase
    {
        private readonly IHubService _hubService;

        public HealthController(IHubService hubService)
        {
            _hubService = hubService;
        }

        /// <summary>
        /// Must be called to fix SimpleInjector error
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult HealthCheck()
        {
            _hubService.InitHub();
            return Ok("Super Health");
        }
    }
}