﻿using System.Threading.Tasks;

namespace FastO.Microservices.Notification.Services
{
    public interface IHubService
    {
        void InitHub();

        Task SendToUserAsync(string userId, object payload);

        Task SendToStoreAsync(string storeId, object payload);
    }
}