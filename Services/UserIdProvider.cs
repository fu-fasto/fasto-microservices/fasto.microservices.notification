﻿using Microsoft.AspNetCore.SignalR;

namespace FastO.Microservices.Notification.Services
{
    public class UserIdProvider : IUserIdProvider
    {
        public string? GetUserId(HubConnectionContext connection)
        {
            return connection.User?.FindFirst("uid")?.Value;
        }
    }
}