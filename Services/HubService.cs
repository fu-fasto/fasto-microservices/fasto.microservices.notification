﻿using System.Threading.Tasks;
using FastO.Microservices.Notification.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.Notification.Services
{
    public class HubService : IHubService
    {
        private readonly ILogger _logger;
        private readonly IHubContext<HubClient> _hubContext;
        private static IHubContext<HubClient> _superHub;

        public HubService(IHubContext<HubClient> hubContext, ILogger<HubService> logger)
        {
            _hubContext = hubContext;
            _logger = logger;
        }

        public void InitHub()
        {
            _superHub = _hubContext;
        }

        public async Task SendToUserAsync(string userId, object payload)
        {
            // _logger.LogInformation("Send to user {UserId}", userId);
            if (_superHub is not null)
            {
                await _superHub?.Clients!.Users(userId).SendAsync(payload.GetType().Name, payload);
            }
        }

        public async Task SendToStoreAsync(string storeId, object payload)
        {
            // _logger.LogInformation("Send to store {StoreId}", storeId);
            if (_superHub is not null)
            {
                await _superHub.Clients.Groups($"STORE/{storeId}").SendAsync(payload.GetType().Name, payload);
            }
        }
    }
}