using System;
using System.Net;
using FastO.Microservices.Notification.Hubs;
using FastO.Microservices.Notification.Services;
using MicroBoost;
using MicroBoost.Auth;
using MicroBoost.Caching;
using MicroBoost.Cqrs;
using MicroBoost.Jaeger;
using MicroBoost.MessageBroker;
using MicroBoost.Metrics;
using MicroBoost.Swagger;
using MicroBoost.WebAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StackExchange.Redis;

namespace FastO.Microservices.Notification
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var builder = services
                .AddMicroBoostBuilder()
                .AddJaeger()
                .AddWebApi()
                .AddCqrs()
                .AddMessageBroker()
                .AddAuth()
                .AddSwaggerDocs();

            var cachingOptions = builder.GetOptions<CachingOptions>("Caching");
            services.AddSingleton<IUserIdProvider, UserIdProvider>();
            services.AddScoped<HubClient>();
            services.AddScoped<IHubService, HubService>();
            services.AddScoped(typeof(IHubActivator<>), typeof(SimpleInjectorHubActivator<>));
            services.AddSignalR(hubOptions =>
                {
                    hubOptions.EnableDetailedErrors = true;
                    hubOptions.KeepAliveInterval = TimeSpan.FromMinutes(1);
                })
                .AddStackExchangeRedis(cachingOptions.ConnectionString,
                    options => { options.Configuration.ChannelPrefix = "_SignalR"; });

            builder.Build();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app
                .UseMetrics()
                .UseJaeger()
                .UseWebApi()
                .UseSwaggerDocs()
                .UseMessageBroker()
                .UseEndpoints(endpoints =>
                {
                    endpoints.MapHub<HubClient>("/hub", options =>
                    {
                        options.Transports =
                            HttpTransportType.WebSockets |
                            HttpTransportType.LongPolling;
                    });
                });
        }
    }
}