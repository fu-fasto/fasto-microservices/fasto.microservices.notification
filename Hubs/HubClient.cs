﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.Notification.Hubs
{
    [Authorize]
    public class HubClient : Hub
    {
        private readonly ILogger _logger;

        public HubClient(ILogger<HubClient> logger)
        {
            _logger = logger;
        }

        public override async Task OnConnectedAsync()
        {
            var role = Context.User?.FindFirst(ClaimTypes.Role)?.Value;
            if (role is not null && role.ToUpper().Contains("STORE"))
            {
                var storeId = Context.User?.FindFirst("storeId")?.Value;
                await Groups.AddToGroupAsync(Context.ConnectionId, $"STORE/{storeId}");
                _logger.LogInformation("{Role} of {StoreId} connected!!!", role, storeId);
            }
            else
            {
                _logger.LogInformation("CUSTOMER {UserId} connected!!!", Context.User!.FindFirst("uid")!.Value);
            }
        }

        public override async Task OnDisconnectedAsync(Exception? exception)
        {
            var role = Context.User?.FindFirst(ClaimTypes.Role)?.Value;
            if (role is not null && role.ToUpper().Contains("STORE"))
            {
                var storeId = Context.User?.FindFirst("storeId")?.Value;
                await Groups.RemoveFromGroupAsync(Context.ConnectionId, $"STORE/{storeId}");
                _logger.LogInformation("{Role} of {StoreId} disconnected!!!", role, storeId);
            }
            else
            {
                _logger.LogInformation("CUSTOMER {UserId} disconnected!!!", Context.User!.FindFirst("uid")!.Value);
            }
        }

        public Task SendToUserAsync(string userId, object payload)
        {
            Console.WriteLine(Clients.Users(userId));
            return Clients.Users(userId).SendAsync(payload.GetType().Name, payload);
        }

        public Task SendToStoreAsync(string storeId, object payload)
        {
            Console.WriteLine(Clients.Users($"STORE/{storeId}"));
            return Clients.Group($"STORE/{storeId}").SendAsync(payload.GetType().Name, payload);
        }
        
        public Task SendMessage(string user, string message)
        {
            return Clients.All.SendAsync("ReceiveMessage", user, message);
        }
    }
}