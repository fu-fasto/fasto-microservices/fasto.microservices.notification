﻿using System;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Notification.Events
{
    [Message(MessageScope.External)]
    public class OrderUpdated : IEvent
    {
        [MessageKey] public Guid Id { get; set; }

        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }

        public int State { get; set; }

        public string Note { get; set; }
    }
}