﻿using System;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Notification.Events
{
    [Message(MessageScope.External)]
    public class ReservationCreated : IEvent
    {
        [MessageKey] public Guid Id { get; set; }

        public string Code { get; set; }
        
        public Guid StoreId { get; set; }

        public Guid CustomerId { get; set; }
        
        public string StoreName { get; set; }
        
        public string CustomerName { get; set; }
        
        public int PeopleNumber { get; set; }

        public DateTimeOffset ExpectedTime { get; set; }

        public int State { get; set; }

        public string Note { get; set; }
    }
}