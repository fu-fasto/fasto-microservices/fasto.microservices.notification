﻿using System;
using MicroBoost.Events;
using MicroBoost.MessageBroker;

namespace FastO.Microservices.Notification.Events
{
    [Message(MessageScope.External)]
    public class OrderCreated : IEvent
    {
        [MessageKey] public Guid Id { get; set; }
        
        public string Code { get; set; }
        public Guid StoreId { get; set; }
        
        public Guid CustomerId { get; set; }
        
        public Guid VisitId { get; set; }
        
        public Guid ItemId { get; set; }
        
        public string Name { get; set; }
        
        public decimal Price { get; set; }
        
        public int Quantity { get; set; }
        
        public int State { get; set; }
        
        public DateTimeOffset ExpectedTime { get; set; }
        
        public string Note { get; set; }
    }
}