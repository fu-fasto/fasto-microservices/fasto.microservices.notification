﻿using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Notification.Services;
using MicroBoost.Events;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.Notification.Events.Handlers
{
    public class OrderCreatedHandler : IEventHandler<OrderCreated>
    {
        private readonly ILogger _logger;
        private readonly IHubService _hubService;

        public OrderCreatedHandler(ILogger<OrderCreatedHandler> logger, IHubService hubService)
        {
            _logger = logger;
            _hubService = hubService;
        }

        public async Task Handle(OrderCreated orderCreated, CancellationToken cancellationToken)
        {
            await _hubService.SendToStoreAsync(orderCreated.StoreId.ToString(), orderCreated);
            await _hubService.SendToUserAsync(orderCreated.CustomerId.ToString(), orderCreated);

            _logger.LogInformation("Sent {Event} {Id} to client", nameof(OrderCreated), orderCreated.Id);
        }
    }
}