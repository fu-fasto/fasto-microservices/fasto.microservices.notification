﻿using System.Threading;
using System.Threading.Tasks;
using FastO.Microservices.Notification.Services;
using MicroBoost.Events;
using Microsoft.Extensions.Logging;

namespace FastO.Microservices.Notification.Events.Handlers
{
    public class PaymentTransactionCompletedHandler : IEventHandler<PaymentTransactionCompleted>
    {
        private readonly ILogger _logger;
        private readonly IHubService _hubService;

        public PaymentTransactionCompletedHandler(ILogger<PaymentTransactionCompletedHandler> logger, IHubService hubService)
        {
            _logger = logger;
            _hubService = hubService;
        }

        public async Task Handle(PaymentTransactionCompleted paymentTransactionCompleted, CancellationToken cancellationToken)
        {
            await _hubService.SendToStoreAsync(paymentTransactionCompleted.StoreId.ToString(), paymentTransactionCompleted);
            await _hubService.SendToUserAsync(paymentTransactionCompleted.CustomerId.ToString(), paymentTransactionCompleted);

            _logger.LogInformation("Sent {Event} {Id} to client", nameof(PaymentTransactionCompleted), paymentTransactionCompleted.Id);
        }
    }
}